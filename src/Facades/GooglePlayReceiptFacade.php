<?php

namespace Appitized\Purchase\Facades;

use Illuminate\Support\Facades\Facade;

class GooglePlayReceiptFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'google_receipt';
    }
}
