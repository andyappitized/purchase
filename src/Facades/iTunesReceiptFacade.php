<?php

namespace Appitized\Purchase\Facades;

use Illuminate\Support\Facades\Facade;

class iTunesReceiptFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'itunes_receipt';
    }
}
