<?php

namespace Appitized\Purchase\Validation;

use Google_Auth_AssertionCredentials as GoogleAssertionCredentials;
use Google_Client as GoogleClient;
use Google_Service_AndroidPublisher as AndroidPublisherService;

class GooglePlayReceipt
{

    protected $purchaseToken = '';
    protected $client;
    protected $service;
    const SCOPE = 'https://www.googleapis.com/auth/androidpublisher';

    public function __construct()
    {
        $this->setClient();
        $this->setAndroidService();
    }

    protected function setClient()
    {
        $this->client = new GoogleClient();
        $this->client->setApplicationName(config('purchase.settings.google.application_name'));
        $p12Key = file_get_contents(config('purchase.settings.google.p12'));

        $assertionCredentials = new GoogleAssertionCredentials(
          config('purchase.settings.google.service_account_email'),
          self::SCOPE,
          $p12Key
        );
        $this->client->setAssertionCredentials($assertionCredentials);
    }

    public function withPurchaseToken($purchaseToken)
    {
        $this->purchaseToken = $purchaseToken;

        return $this;
    }

    protected function setAndroidService()
    {
        $this->service = new AndroidPublisherService($this->client);
    }

    protected function getPackageName()
    {
        return config('purchase.settings.google.package_name');
    }

    public function validatePurchase($productId)
    {
        try {
            $product = $this->service->purchases_products->get($this->getPackageName(),
              $productId, $this->purchaseToken);
        } catch (\Google_Service_Exception $exception) {
            return response()->json(['message' => $exception->getMessage()],
              $exception->getCode());
        }

    }

    public function validateSubscription($subscriptionId)
    {
        try {
            $subscription = $this->service->purchases_subscriptions->get($this->getPackageName(),
              $subscriptionId, $this->purchaseToken);
        } catch (\Google_Service_Exception $exception) {
            return response()->json(['message' => $exception->getMessage()],
              $exception->getCode());
        }

    }

}
