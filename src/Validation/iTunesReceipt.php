<?php

namespace Appitized\Purchase\Validation;

use Appitized\Purchase\Responses\iTunesResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class iTunesReceipt
{
    const ENDPOINT_SANDBOX = 'https://sandbox.itunes.apple.com/verifyReceipt';
    const ENDPOINT_PRODUCTION = 'https://buy.itunes.apple.com/verifyReceipt';

    protected $endpoint;
    protected $receipt = '';
    protected $receiptData;
    protected $password = '';
    protected $client = null;

    public function __construct()
    {
        $this->setEnvironment(config('purchase.settings.itunes.environment'));
    }

    public function setEnvironment($environment)
    {
        $environment = strtolower($environment);
        switch ($environment) {
            case 'sandbox':
                $this->endpoint = self::ENDPOINT_SANDBOX;
                break;
            case 'production':
                $this->endpoint = self::ENDPOINT_PRODUCTION;
                break;
            default:
                throw new \Exception('Environment not supported');
                break;
        }

        return $this;
    }

    /**
     * Get the Guzzle client for HTTP requests
     *
     * @return \GuzzleHttp\Client|null
     */
    protected function getClient()
    {
        if ($this->client == null) {
            $this->client = new Client(['base_uri' => $this->endpoint]);
        }

        return $this->client;
    }

    public function withReceiptData($receipt, $password = null)
    {
        if(!$this->isBase64Encoded($receipt))
        {
            throw new Exception('Receipt data is invalid');
        }

        $this->receipt = $receipt;
        $this->password = $password;

        return $this;
    }

    public function validate()
    {
        $httpResponse = $this->getClient()->request('POST', '/verifyReceipt', [
          'json' => [
            'receipt-data' => $this->receipt,
            'password' => $this->password
          ]
        ]);
        if ($this->requestFailed($httpResponse)) {
            throw new \Exception('Unable to get response from iTunes server');
        }
        $response = new iTunesResponse($httpResponse);

        return $response->isValidReceipt();
    }

    protected function isBase64Encoded($data)
    {
        return base64_decode($data, true);
    }

    protected function requestFailed(Response $response)
    {
        return $response->getStatusCode() != 200;
    }

}
