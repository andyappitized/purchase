<?php

namespace Appitized\Purchase;

use Appitized\Purchase\Validation\GooglePlayReceipt;
use Appitized\Purchase\Validation\iTunesReceipt;
use Illuminate\Support\ServiceProvider;

class PurchaseServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes([
          __DIR__.'/config/settings.php' => config_path('purchase/settings.php'),
        ]);
        $this->mergeConfigFrom(__DIR__.'/config/settings.php', 'purchase.settings');
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('itunes_receipt', function()
        {
            return new iTunesReceipt();
        });

        $this->app->bind('google_receipt', function()
        {
            return new GooglePlayReceipt();
        });
    }
}
