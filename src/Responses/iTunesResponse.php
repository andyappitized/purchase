<?php

namespace Appitized\Purchase\Responses;

use GuzzleHttp\Psr7\Response;

class iTunesResponse
{
    const ERRORS = [
      'Appstore cannot read data' => 21000,
      'Data is malformed' => 21002,
      'Receipt not authenticated' => 21003,
      'Shared secret does not match' => 21004,
      'Receipt server is unavailable' => 21005,
      'Receipt is valid but subscription has expired' => 21006,
      'Sandbox receipt has been sent to production' => 21007,
      'Production receipt has been sent to sandbox' => 21008,
    ];

    const RESULT_OK = 0;
    const RESULT_APPSTORE_CANNOT_READ = 21000;
    const RESULT_DATA_MALFORMED = 21002;
    const RESULT_RECEIPT_NOT_AUTHENTICATED = 21003;
    const RESULT_SHARED_SECRET_NOT_MATCH = 21004;
    const RESULT_RECEIPT_SERVER_UNAVAILABLE = 21005;
    const RESULT_RECEIPT_VALID_BUT_SUB_EXPIRED = 21006;
    const RESULT_SANDBOX_RECEIPT_SENT_TO_PRODUCTION = 21007;
    const RESULT_PRODUCTION_RECEIPT_SENT_TO_SANDBOX = 21008;

    public function __construct(Response $response)
    {
        $this->response = json_decode($response->getBody()->getContents());
    }

    public function isValidReceipt()
    {
        if (self::RESULT_OK != $this->response->status) {
            throw new \Exception(array_search($this->response->status, self::ERRORS));
        }

        return $this->response->receipt;
    }
}
