<?php

return [
  'itunes' => [
    'environment' => 'sandbox',
  ],
  'google' => [
    'application_name' => '',
    'service_account_email' => '116775786389-qccpemkpde1bjjnfdqbfiiafrboiibbg@developer.gserviceaccount.com',
    'package_name' => 'com.appitized.hiredapp',
    'p12' => app_path() . '/private/key.p12',
  ]
];
