# Appitized Purchase

The package provides validation for iTunes and Google Play receipts.


## Install

You can pull in the package via composer:
``` bash
$ composer require appitized/purchase
```

Next up, the service provider must be registered:

```php
// Laravel5: config/app.php
'providers' => [
    ...
    Appitized\Purchase\PurchaseServiceProvider::class,

];
```

If you want to make use of the facade you must install it as well:

```php
// config/app.php
'aliases' => [
    ...
    'iTunesReceipt' => Appitized\Purchase\Facades\iTunesReceiptFacade::class,
    'GooglePlayReceipt' => Appitized\Purchase\Facades\GooglePlayReceiptFacade::class,
];
```

If you want to change the config file:

```bash
php artisan vendor:publish --provider="Appitized\Purchase\PurchaseServiceProvider"
```

This is the contents of the published file:

```php

return [
  'itunes' => [
    'environment' => 'sandbox',
  ],
  'google' => [
    'application_name' => '',
    'service_account_email' => '',
    'package_name' => '',
    'p12' => app_path() . '',
  ]
];
```

## iTunes Usage

The following will return a valid iTunes receipt:

```php
$receipt = iTunesReceipt::withReceiptData($receipt)
	->validate();
```

## Google Play Usage

The following will validate a product receipt:

```php
$receipt = GooglePlayReceipt::withPurchaseToken($request->purchase_token)
	->validatePurchase($product_id);
```

or a subscription receipt:

```php
$receipt = GooglePlayReceipt::withPurchaseToken($request->purchase_token)
	->validateSubscription($subscription_id);
```
